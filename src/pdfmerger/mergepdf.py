import sys
import os
import logging
import argparse
import pandas as pd
from pathlib import Path
from distutils.util import strtobool

from pdfmerger.pdf import (PdfFileWriter, PdfFileReader)

try:
    from pdfmerger import __version__
except ModuleNotFoundError:
    __version__ = "unknown"


DEFAULT_PDF_NAME = "merged.pdf"

# set up the logger
logging.basicConfig(format="%(levelname)6s : %(message)s", level=logging.INFO)
logger = logging.getLogger(__name__)


def merge_pdf_files(filenames, outfile):
    """
    Merge the pdf file stored in the list filenames

    Parameters
    ----------
    filenames: list
        list of files to merge
    outfile: Path
        output pdf file

    """

    logger.debug("Start collecting pdf files")
    pdf_out = PdfFileWriter()
    all_in_streams = list()

    for cnt, file_n in enumerate(filenames):
        logger.info("Adding pdf file {}".format(file_n))
        # note that you can not use the with/as construction here because all input stream
        # need to be open at the moment we cal the write statement to the out stream
        in_stream = open(file_n.name, "rb")
        all_in_streams.append(in_stream)
        pdf_in = PdfFileReader(in_stream)

        for page_num in range(pdf_in.numPages):
            pdf_out.addPage(pdf_in.getPage(page_num))

    logger.info("Writing to pdf file {}".format(outfile))

    with open(outfile.resolve(), "wb") as out_stream:
        pdf_out.write(out_stream)

    # close the input streams. Not really needed in python but just to emphasis that we need to
    # keep the streams open at the moment of writing to the pdf file
    for in_stream in all_in_streams:
        in_stream.close()

    logger.debug("Done")


def get_and_check_files(filenames, directory_name, output_filename):
    """
    Get all the file names, including the once with globbing stars

    Parameters
    ----------
    filenames: list of str
        List of file names. May contains files like blabla*.pdf
    directory_name: str or None
        Name of the directory. If not given, use the directory of the first input file
    output_filename: str
        Name of the output file

    Returns
    -------
    (file_list, out_dir):
        tuple with a  list of file names and an output filename

    """

    out_file = None
    if output_filename is not None:
        out_file = Path(os.path.expanduser(output_filename))
        out_dir = Path(out_file.parent)
    elif directory_name is not None:
        out_dir = Path(os.path.expanduser(directory_name))
    else:
        out_dir = None

    file_list = list()
    for file_n in filenames:
        dir_name, file_glob = os.path.split(file_n)
        directory = Path(dir_name)
        all_files = directory.glob(file_glob)
        for this_file in all_files:
            if not this_file.exists():
                raise FileNotFoundError("Could not find file {}".format(this_file.name))
            else:
                file_list.append(dir_name / this_file)

        if out_dir is None:
            out_dir = directory

    if not out_dir.exists():
        raise IsADirectoryError("Could not find output directory {}".format(out_dir))

    if output_filename is not None:
        outfile = out_dir / out_file.name
    else:
        outfile = out_dir / DEFAULT_PDF_NAME

    # make sure we have a pdf file
    outfile = outfile.with_suffix(".pdf")

    return file_list, outfile


def _parse_the_command_line_arguments(args):
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    # parse the command line to set some options2
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    parser = argparse.ArgumentParser(description='Merge multiple PDF files',
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("filenames", nargs="+", help="List of pdf files the merge")
    parser.add_argument("--version", help="Show the current version", action="version",
                        version=f"{__version__}")
    parser.add_argument('-d', '--debug', help="Print lots of debugging statements",
                        action="store_const", dest="log_level", const=logging.DEBUG,
                        default=logging.INFO)
    parser.add_argument('-v', '--verbose', help="Be verbose", action="store_const",
                        dest="log_level", const=logging.INFO)
    parser.add_argument('-q', '--quiet', help="Be quiet: no output", action="store_const",
                        dest="log_level", const=logging.WARNING)
    parser.add_argument('-o', '--output_filename', help="Name of the output file")
    parser.add_argument('--directory', help="Name of directory to store the merged file.")
    parser.add_argument('--overwrite', help="Overwrite the output file if it already exist ",
                        action="store_true")
    # parse the command line
    parsed_arguments = parser.parse_args(args)

    return parsed_arguments, parser


def okay_to_continue(question, default_answer="y"):
    """
    A small function to get some user feed back based on the question

    Parameters
    ----------
    question: str
        Question to ask
    default_answer: str
        Use this answer if nothing is given

    Returns
    -------
    bool
        True if it is ok to continue

    """
    while True:
        ans = input(question)
        if ans is None or ans == "":
            ans = default_answer
        try:
            is_okay = strtobool(ans)
        except ValueError:
            print("Did not recognise the answer")
        else:
            break

    return is_okay


def main(args_in):
    args, parser = _parse_the_command_line_arguments(args_in)

    # change log level
    logger.setLevel(args.log_level)
    logger.debug("{:10s}: {}".format("Running", sys.argv))
    logger.debug("{:10s}: {}".format("Version", __version__))
    logger.debug("{:10s}: {}".format("Directory", os.getcwd()))

    script_name = os.path.basename(sys.argv[0])
    start_time = pd.to_datetime("now")
    message = "Start {script} (v: {version}) at {start_time}:\n{cmd}".format(script=script_name,
                                                                             version=__version__,
                                                                             start_time=start_time,
                                                                             cmd=sys.argv[:])
    logger.debug(message)

    file_list, out_file = get_and_check_files(args.filenames,
                                              directory_name=args.directory,
                                              output_filename=args.output_filename)

    # check if we are allowed to overwrite the file if it is already there
    if out_file.exists() and not args.overwrite:
        is_okay = okay_to_continue("Output file {} exists. Overwrite? [Y/n]".format(out_file))
        if not is_okay:
            logger.info("Quit without writing a new pdf file")
            sys.exit(0)

    merge_pdf_files(file_list, out_file)


def _run():
    """Entry point for console_scripts
    """
    main(sys.argv[1:])


if __name__ == '__main__':
    _run()
