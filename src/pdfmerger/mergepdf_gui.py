# -*- coding: utf-8 -*-
"""
Created on Tue Jun 28 10:52:09 2016

@author: LGRN

Allows you to select two .pdf files, merges them
and writes the result into the same folder as the first file
with the name merged.pdf

This script has been optimized for use in windows.
In other operating systems path names may be processed incorrectly
and the "open destination folder" button will likely not work.
"""

import os.path
import subprocess
from pdfmerger.pyqt_import import *

from pdfmerger.pdf import (PdfFileReader, PdfFileWriter)


# IF PyPDF2 IS NOT FOUND: add three commented out lines below
# thisfile = os.path.abspath(__file__)
# thisdir = os.path.dirname(thisfile)
# sys.path.append(thisdir)

class PdfSelector(QWidget):
    """ The GUI for pdf merging """

    def __init__(self, parent=None):
        """ sets up main screen when it is created """
        super(PdfSelector, self).__init__(parent)

        self.setWindowTitle("PDF file merger")

        # create three buttons with text boxes below them
        self.btn1 = QPushButton("Choose file 1")
        self.btn1.clicked.connect(self.getfile1)
        self.path1 = QLineEdit()

        self.btn2 = QPushButton("Choose file 2")
        self.btn2.clicked.connect(self.getfile2)
        self.path2 = QLineEdit()

        self.btn3 = QPushButton("Combine")
        self.btn3.clicked.connect(self.combine)
        self.path3 = QLineEdit()
        # the third text box cannot be edited by the user
        self.path3.setReadOnly(True)

        self.btn4 = QPushButton("Open destination folder")
        self.btn4.clicked.connect(self.openfolder)

        # layout all the objects vertically
        layout = QVBoxLayout()
        layout.addWidget(self.btn1)
        layout.addWidget(self.path1)
        layout.addWidget(self.btn2)
        layout.addWidget(self.path2)
        layout.addWidget(self.btn3)
        layout.addWidget(self.path3)
        layout.addWidget(self.btn4)
        self.setLayout(layout)

    def getfile1(self):
        """
        If you push button 1, select a file from a browser 
        and display it in the texbox below
        starting from the path in the other textbox or c:\
        """
        if self.path2.text():
            basepath = os.path.dirname(self.path2.text())
        else:
            basepath = 'c:\\'
        file_name = QFileDialog.getOpenFileName(self, 'Open file',
                                                basepath, "pdf files (*.pdf)")
        if isinstance(file_name, tuple):
            file_name = file_name[0]
        if file_name:
            self.path1.setText(file_name.replace('/', '\\'))

    def getfile2(self):
        """
        If you push button 2, select a file from a browser 
        and display it in the texbox below
        starting from the path in the other textbox or c:\
        """
        if self.path1.text():
            basepath = os.path.dirname(self.path1.text())
        else:
            basepath = 'c:\\'
        file_name = QFileDialog.getOpenFileName(self, 'Open file',
                                                basepath, "QtGui files (*.pdf)")
        if isinstance(file_name, tuple):
            file_name = file_name[0]
        if file_name:
            self.path2.setText(file_name.replace('/', '\\'))

    def combine(self):
        """
        If you push button 3, check if two files have been chosen
        and combine them into one pdf if they have
        """
        file1 = self.path1.text()
        file2 = self.path2.text()
        file1_exists = os.path.isfile(file1)
        file2_exists = os.path.isfile(file2)
        if file1_exists and file2_exists:
            outputdir = os.path.dirname(file1)
            outputfile = pdfmerger(file1, file2, outputdir)
            outputtext = (outputfile
                          + ' written to '
                          + outputdir
                          + '. Choose another file and Combine to add another file.')
            self.path3.setText(outputtext.replace('/', '\\'))
            # Now set fields for potential third file to merge
            outputpath = os.path.join(outputdir, outputfile)
            self.path1.setText(outputpath.replace('/', '\\'))
            self.path2.clear()
            self.btn1.setText('Base file')
            self.btn2.setText('Choose another file')
        else:
            self.path3.setText('Input two valid file names!')

    def openfolder(self):
        """
        If you push button 4, open the directory specified for file1
        this corresponds to where the output file will be placed.        
        """
        if self.path1.text():
            outputdir = os.path.dirname(self.path1.text())
            if os.path.isdir(outputdir):
                openstring = 'explorer "' + outputdir + '"'
                subprocess.Popen(openstring)


def append_pdf(input, output):
    """ Appends files to the output file """
    [output.addPage(input.getPage(page_num)) for page_num in range(input.numPages)]


def pdfmerger(inputfile1, inputfile2, outputfolder):
    """ Merges two pdf files into one in given outputfolder """
    # Determine output file name, giving it a number if
    # the default file name ('merged.pdf') aleady exists.
    outputfile = "merged"  # default name of the output file
    outputpath = outputfolder + "/" + outputfile
    if os.path.isfile(outputpath + ".pdf"):
        filenumber = 1
        while os.path.isfile(outputpath + str(filenumber) + ".pdf"):
            filenumber += 1
        outputfile += str(filenumber)
        outputpath += str(filenumber)
    outputpath += ".pdf"
    outputfile += ".pdf"

    # Create an object pdf pages are appended to
    output = PdfFileWriter()

    # Appending pdf-pages from two different files
    with open(inputfile1, "rb") as pdf1, open(inputfile2, "rb") as pdf2:
        append_pdf(PdfFileReader(pdf1), output)
        append_pdf(PdfFileReader(pdf2), output)

        # Writing all the collected pages to the output file
        output.write(open(outputpath, "wb"))
        print("Merged file written to", outputpath.replace('/', '\\'))

    return outputfile


def main():
    app = QApplication(sys.argv)
    ex = PdfSelector()
    ex.resize(600, 200)
    ex.show()
    sys.exit(app.exec_())


if __name__ == '__main__':
    main()
