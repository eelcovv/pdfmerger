=========
Changelog
=========

Version 0.1
===========

- Turned pdfmerger in a python package
- Updated for python 3.6 + qt5
